"""provides the Print convenience class"""
import sys

class Print:    
    """contains convenience wrappers around the 'print' function"""
    flush = False

    @staticmethod
    def line(string):
        """'print line' prints a string to stdout with a newline appended"""
        print(string, flush=Print.flush)

    @staticmethod
    def inline(string):
        """'print inline' prints a string to stdout without appending a newline"""
        print(string, flush=Print.flush, end='')

    @staticmethod
    def error(string):
        """'print error' prints a string to stderr with a newline appended"""
        print(string, flush=Print.flush, file=sys.stderr)