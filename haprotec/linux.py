"""provides commom methods for linux systems"""
import os
import platform
import time
import re

def check_os():
    """returns true if the script is run under a linux os"""
    return os.name == 'posix' and platform.system() == 'Linux'

def set_hostname(prefix, sysroot):
    """sets the hostname on a linux system"""
    hostname = "{}-{}".format(prefix, int(time.time()))
    print("setting hostname to {}...".format(hostname), end='', flush=True)
    # overwrite /etc/hostname
    with open(os.path.join(sysroot, "etc/hostname"), mode='w') as fp:
        fp.write("{}\n".format(hostname))
    # replace hostname in /etc/hosts
    with open(os.path.join(sysroot, "etc/hosts"), mode='r+') as fp:
        content = fp.read()
        pattern = re.compile(r'(127\.0\.1\.1\s+)([^\n\r]+)')
        content = pattern.sub(r'\g<1>{}'.format(hostname), content)
        fp.seek(0)
        fp.truncate()
        fp.write(content)
    print("done\n", flush=True)
