"""provides a progress meter for file transfers"""
import time
import math

class Progress:
    """ Outputs a textual progress meter for large file operations"""
    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-few-public-methods

    def __init__(self, file_size):
        """ Creates a new instance and sets the start time to the current time """
        self.file_size = file_size
        self.file_size_proc = 0
        self.file_size_lastblock = 0
        self.starttime = time.monotonic()
        self.blocktime = self.starttime
        self.last_dot = 0
        self.last_status = 0
        self.last_space = 0
        # print beginning of progress meter
        print("{:10,.0f}K ".format(self.file_size_proc/1024), end="", flush=True)

    def block(self, block):
        """ Must be called after a block has completed. Triggers the output """
        self.file_size_proc += len(block)
        #print status output
        current_percents = self.file_size_proc * 100. / self.file_size
        dot = math.floor(current_percents * 5)
        if dot > self.last_dot:
            for x in range(dot - self.last_dot):
                print(".", end="", flush=True)
                dot_percents = (self.last_dot + x + 1) / 5
                # print a space after each completed percent
                if math.floor(dot_percents) > self.last_space:
                    print(" ", end="", flush=True)
                    self.last_space = math.floor(dot_percents)
                # print detailed status every 10% and go to next line
                if (not math.floor(dot_percents) % 10
                        and math.floor(dot_percents) != self.last_status):
                    self.last_status = math.floor(dot_percents)
                    mtime = time.monotonic()
                    duration = mtime - self.blocktime
                    self.blocktime = mtime
                    speed = (self.file_size_proc - self.file_size_lastblock) / (duration * 1024)
                    self.file_size_lastblock = self.file_size_proc
                    status = '{:3.0f}% {:6.1f}s {:7.2f}kB/s'.format(dot_percents, mtime-self.starttime, speed)
                    print(status, flush=True)
                    if self.file_size == self.file_size_proc:
                        # transfer complete, print statistics
                        print("transferred {:,.1f}K in {:.1f} seconds".format(self.file_size_proc/1024, mtime-self.starttime), flush=True)
                    else:
                        # start next line of meter
                        print("{:10,.0f}K ".format(self.file_size_proc/1024), end="", flush=True)
            self.last_dot = dot
            