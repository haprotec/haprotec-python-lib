"""contains miscellaneous often utilized methods"""
import os
import urllib.request
import hashlib
import tarfile
import zipfile
from .progress import Progress

def copy_file(src, dst):
    """copies a file from 'src' to 'dst' """
    file_size = os.path.getsize(src)
    print("Copying '{}' to '{}'".format(src, dst), flush=True)
    with open(src, 'rb') as fsrc:
        with open(dst, 'wb') as fdst:
            prog_meter = Progress(file_size)
            while True:
                buffer = fsrc.read(8192)
                if not buffer:
                    break
                prog_meter.block(buffer)
                fdst.write(buffer)
    print("done", flush=True)

def download_file(url, target_file):
    """downloads a  file from 'url' to 'target_file'"""
    u = urllib.request.urlopen(url)
    file_size = int(u.getheader("Content-Length"))
    print("Downloading: {} ({:.1f}K)".format(url, file_size/1024), flush=True)
    with open(target_file, 'wb') as f:
        prog_meter = Progress(file_size)
        while True:
            buffer = u.read(8192)
            if not buffer:
                break
            prog_meter.block(buffer)
            f.write(buffer)
    print("Download completed", flush=True)

def sha1_checksum(file):
    """ Calculate the sha1 checksum of a given file """
    sha1 = hashlib.sha1()
    chunksize = 65536
    with open(file, 'rb') as f:
        while True:
            data = f.read(chunksize)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()

def verify_checksum(checksum_string, file):
    """ searches 'checksum_string' for the the calculated checksum of 'file'"""
    checksum = sha1_checksum(file)
    print("checksum of zip file is '{}'".format(checksum), flush=True)
    return checksum in checksum_string

def untar_file(archive, file):
    """ Extract img file from (compressed) tar-archive """
    image_dir, _ = os.path.split(file)
    # check if the working dirctory exists and create it if needed
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)
    chunksize = 8192
    with tarfile.open(archive) as tar:
        print("Extracting image ", flush=True)
        member = tar.getmembers()[0]
        with tar.extractfile(member) as tarf:
            with open(file, 'wb') as f:
                prog_meter = Progress(member.size)
                while True:
                    buffer = tarf.read(chunksize)
                    if not buffer:
                        break
                    prog_meter.block(buffer)
                    f.write(buffer)
    print(" done", flush=True)

def unzip_file(archive, file):
    """ Extract file from zip-archive """
    target_dir, _ = os.path.split(file)
    # check if the working dirctory exists and create it if needed
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    chunksize = 8192
    with zipfile.ZipFile(archive, 'r') as zarch:
        print("Inflating image...", flush=True)
        member = zarch.getinfo(zarch.namelist()[0])
        prog_meter = Progress(member.file_size)
        with zarch.open(member) as zipf:
            with open(file, 'wb') as f:
                while True:
                    buffer = zipf.read(chunksize)
                    if not buffer:
                        break
                    prog_meter.block(buffer)
                    f.write(buffer)
    print(" done", flush=True)

def tar_file(file, archive_path):
    """ Compresses the image after completing the build process
    the archive will be named like the 'file' 
    """
    archive_file = os.path.join(archive_path, "{}.tar.xz".format(os.path.splitext(file)[0]))
    with tarfile.open(archive_file, mode='x:xz') as tar:
        print("Compressing file '{}'...".format(file), end='', flush=True)
        tar.add(file)
    print("done", flush=True)
    print("Archive written to {}".format(archive_file), flush=True)
    print("Archive size is {:.1f}K".format(os.path.getsize(archive_file)/1024), flush=True)
    return archive_file
