from setuptools import setup

setup(name='haprotec',
      version='0.1',
      description='python library containing snippets which are commonly used at haprotec GmbH system-automation',
      url='https://gitlab.com/haprotec/haprotec-python-lib',
      author='Alexander Rose',
      author_email='arose@haprotec.de',
      license='MIT',
      packages=['haprotec'],
      zip_safe=False)